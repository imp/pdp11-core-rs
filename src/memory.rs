use std::fmt;

crate struct Memory {
    memory: [u8; 64 * 1024],
}

impl fmt::Debug for Memory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Core Memory (64KB)")
    }
}
