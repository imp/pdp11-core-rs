#![feature(rust_2018_preview)]

pub use crate::core::Register;
use crate::core::Core;
use crate::insns::Instruction;
use crate::memory::Memory;

mod core;
mod memory;
mod insns;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
