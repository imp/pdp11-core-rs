#[derive(Debug)]
crate enum Instruction {
    Halt,
    Wait,
    Reset,
    Nop,
}
