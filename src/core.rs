use crate::Memory;

pub type Register = u16;

#[derive(Debug)]
crate struct Core {
    memory: Memory,
    r0: Register,
    r1: Register,
    r2: Register,
    r3: Register,
    r4: Register,
    r5: Register,
    r6: Register,
    r7: Register,
}


impl Core {
    fn sp(&self) -> Register {
        self.r6
    }

    fn pc(&self) -> Register {
        self.r7
    }
}
